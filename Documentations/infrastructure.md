## Infrastructure

To host the app, we've set up some AWS services.

### Database

The data persistment is a Postgres Instance on DBS.

The endpoint is : [udacity.cak4x5xiwvg4.us-east-1.rds.amazonaws.com]()

You need my credentials to connect inside to.

### API

The API is a simple express server host on Elastic Beanstalk (Second Project)

The endpoint is : [Store-env.eba-cf6rgsab.us-east-1.elasticbeanstalk.com ](http://store-env.eba-cf6rgsab.us-east-1.elasticbeanstalk.com/)

You can use the API with postman as explain into Requirment.md on the Backend Repo

### Frontend

The Frontend app use the framwork Angular host on S3 (Third Project)

The endpoint is : [http://udacity-store.s3-website-us-east-1.amazonaws.com]()

You can use this app feel free

### Images

Only Free rights Images are used on this project
