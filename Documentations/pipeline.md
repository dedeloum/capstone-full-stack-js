## Pipeline CI/CD

We use CircleCI as continuous tool integration

As rubrics on previous projects, the project 2 have CI requirement contrary to the project 3. So we have set up CD for the backend part.

### Process of the pipeline

1. Thirty tests are launched
2. If they passed and we are on the main branch, TS are exported in JS
3. After that, AWS CLI is configured to use Beanstalk
4. If all good, the "eb-deploy" cmd is called to deploy the code on production

### Setting ENV

[https://app.circleci.com/pipelines/github/Monrocq/nd0067-c2-creating-an-api-with-postgresql-and-express-project-starter/43/workflows/77f6549b-08ab-42fe-a357-934e357d9216/jobs/97/parallel-runs/0/steps/0-110Link to CircleCI Log](https://app.circleci.com/pipelines/github/Monrocq/nd0067-c2-creating-an-api-with-postgresql-and-express-project-starter/43/workflows/77f6549b-08ab-42fe-a357-934e357d9216/jobs/97/parallel-runs/0/steps/0-110)

### CircleCI Badge

#### API Part

[![CircleCI](https://circleci.com/gh/Monrocq/nd0067-c2-creating-an-api-with-postgresql-and-express-project-starter/tree/master.svg?style=svg)](https://circleci.com/gh/Monrocq/nd0067-c2-creating-an-api-with-postgresql-and-express-project-starter/tree/master)

#### Frontend Part

[![CircleCI](https://circleci.com/gh/Monrocq/nd-0067-c3/tree/main.svg?style=svg)](https://circleci.com/gh/Monrocq/nd-0067-c3/tree/main)

#### Full-Stack App (API + Frontend)

[![CircleCI](https://circleci.com/bb/dedeloum/capstone-full-stack-js/tree/master.svg?style=svg)](https://circleci.com/bb/dedeloum/capstone-full-stack-js/tree/master)
