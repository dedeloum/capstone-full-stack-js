# Capstone JS Full-stack Path

This is the capstone project from the Udacity Path : JS Full-Stack.

## Context

The purpose of this capstone is to check if we have the skills to deploy an application into the cloud.

I got helped from previous projects as the rubrics allowed it

## Technos used

For this project we've used two services :

- AWS (Cloud Provider)
- CircleCI (For the Continius Integration)

## The repositories of the code

| Backend                                                                                                                | Frontend                                  |
| ---------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| [Backend Repository](https://github.com/Monrocq/nd0067-c2-creating-an-api-with-postgresql-and-express-project-starter) | [https://github.com/Monrocq/nd-0067-c3]() |

##### includes package.json & config.yml respectively
